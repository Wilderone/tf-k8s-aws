
module "network" {
  source = "./modules/network"

}

module "security_groups" {
  source     = "./modules/security_groups"
  depends_on = [module.network]
  vpc_id     = module.network.vpc_id
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = file("/home/prim/.ssh/id_rsa.pub")
}

resource "aws_lb" "k8s-lb" {

  name               = "k8s-lb"
  internal           = false
  load_balancer_type = "network"


  subnet_mapping {
    subnet_id     = module.network.public_subnets[0]
    allocation_id = module.network.eip_lb
  }

  tags = {
    Name = "l8s-lb"
  }
}

resource "aws_lb_target_group" "k8s-target" {
  count    = length(var.target_grps)
  name     = "k8s-target-${var.target_grps[count.index].port}"
  port     = var.target_grps[count.index].port
  protocol = var.target_grps[count.index].protocol
  vpc_id   = module.network.vpc_id
}

# resource "aws_lb_target_group_attachment" "target_attachment" {
#   for_each = {
#     for pair in setproduct(keys(aws_lb_target_group.k8s-target), aws_isntance.master.*.id) :
#     "${pair[0]}:${pair[1]}" => {
#       target_group = aws_lb_target_group.k8s-target[pair[0]]
#       instance_id  = pair[1]
#     }
#   }
#   # count = length(aws_lb_target_group.k8s-target)
#   target_group_arn = each.value.aws_lb_target_group.k8s-target[count.index].arn

# }

# resource "aws_lb_target_group_attachment" "trg_attachment" {
#   for_each = {
#     for pair in setproduct(aws_lb_target_group.k8s-target.*.id, aws_instance.master.*.id) :
#     "${pair[0]}:${pair[1]}" => {
#       target_group = aws_lb_target_group.k8s-target[pair[0]]
#       instance_id  = pair[1]
#     }
#   }

#   target_group_arn = each.value.target_group.arn
#   target_id        = each.value.instance_id
#   port             = each.value.target_group.target_port
#   depends_on       = [aws_instance.master]
# }



resource "aws_instance" "master" {
  count                       = var.master_instances.count
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.medium"
  vpc_security_group_ids      = module.security_groups.sg_id
  subnet_id                   = module.network.public_subnets[0]
  user_data                   = file("./templates/install_master.sh")
  associate_public_ip_address = true
  key_name                    = "deployer-key"
  tags = {
    Name = "${var.master_instances.hostname}-${count.index + 1}"
  }

}

resource "aws_instance" "worker" {
  count                       = var.worker_instances.count
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t2.medium"
  vpc_security_group_ids      = module.security_groups.sg_id
  subnet_id                   = module.network.public_subnets[0]
  user_data                   = file("./templates/install_master.sh")
  associate_public_ip_address = true
  key_name                    = "deployer-key"
  tags = {
    Name = "${var.worker_instances.hostname}-${count.index + 1}"
  }

}
