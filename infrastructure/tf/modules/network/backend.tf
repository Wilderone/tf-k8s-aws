terraform {
  backend "s3" {
    bucket = "wilderone-terraform-learn-bucket"
    key    = "dev/network/terraform.tfstate"
    region = "eu-central-1"
  }
}
