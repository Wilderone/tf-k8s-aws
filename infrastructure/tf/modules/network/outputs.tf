output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "vpc_cidr" {
  value = aws_vpc.vpc[*].cidr_block
}

output "IG" {
  value = aws_internet_gateway.main_gw.id
}

output "public_subnets" {
  value = aws_subnet.public_subnets[*].id
}

output "private_subnets" {
  value = aws_subnet.private_subnets[*].id
}

output "eip_lb" {
  value = aws_eip.eip_lb.id
}



