terraform {
  backend "s3" {
    bucket = "wilderone-terraform-learn-bucket"
    key    = "/dev/security_groups/terraform.tfstate"
    region = "eu-central-1"
  }
}
