resource "aws_security_group" "k8s_sg" {
  count = length(var.sg_groups)

  name        = var.sg_groups[count.index].name
  description = var.sg_groups[count.index].description
  vpc_id      = length(var.vpc_id) == length(var.sg_groups) ? element(var.vpc_id, count.index) : var.vpc_id

  dynamic "ingress" {
    for_each = var.sg_groups[count.index].allowed_ports
    content {
      description = "allow port ${ingress.value}"
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.sg_groups[count.index].protocol
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.sg_groups[count.index].name
  }

}
