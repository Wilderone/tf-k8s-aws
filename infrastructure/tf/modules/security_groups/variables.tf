variable "sg" {
  default = {
    name        = "k8s_sg"
    description = "Security Group for kubernetes cluster"
  }
  type = map
}

variable "sg_groups" {
  type = list
  default = [
    {
      name          = "vpc_sg"
      description   = "Security Group for VPC"
      allowed_ports = ["80", "443", "22", "8080", "6443", "10250", "10251", "10252", "2379", "2380"]
      protocol      = "tcp"
    },
    {
      name          = "nodes_sg"
      description   = "Security Group for k8s nodes"
      allowed_ports = [0]
      protocol      = "-1"
    }
  ]
}


variable "vpc_id" {}


