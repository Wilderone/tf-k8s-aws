
output "workers_ip" {
  value = aws_instance.worker[*].public_ip
}


output "masters_ip" {
  value = aws_instance.master[*].public_ip
}
