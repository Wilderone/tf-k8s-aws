
terraform {
  backend "s3" {
    bucket = "wilderone-terraform-learn-bucket"
    key    = "dev/main/terraform.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region = var.aws_region
}
