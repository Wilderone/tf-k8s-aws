#!/usr/bin/env bash
sudo su -
ufw disable
apt update
swapoff -a; sed -i '/swap/d' /etc/fstab
cat >>/etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common ssh-agent
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt update
apt install -y docker-ce containerd.io

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list

apt update && apt install -y kubeadm kubelet kubectl

# kubeadm init --apiserver-advertise-address=${master_ip} --pod-network-cidr=192.168.0.0/16  --ignore-preflight-errors=all
