variable "aws_region" {
  default = "eu-central-1"
}

variable "master_instances" {
  default = {
    hostname = "master",
    count    = 3
  }
}

variable "worker_instances" {
  default = {
    hostname = "worker",
    count    = 3
  }
}

variable "target_grps" {
  default = [
    {
      port : 80
      protocol : "HTTP"

    },
    {
      port : 8080
      protocol : "HTTP"
    },
    {
      port : 6443
      protocol : "TCP"
    }

  ]

}
